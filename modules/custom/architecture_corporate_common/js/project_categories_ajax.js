/**
 * Ajax reload views.
 * @file
 */
(function ($, Drupal) {

  'use strict';

  /**
   * Views ajax reload.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.viewsAjaxReload = {
    attach: function (context, settings) {
      // @TODO Think about getting a selector from the admin settings page
      // @TODO to make this function more flexible.
      $('.view-display-id-projects_categories .slick__slide').once('views-reload-links').each(this.attachLink);
    },

    attachLink: function (idx, column) {
      if (typeof(drupalSettings.view_custom_settings.view_id) !== "undefined" &&
        typeof(drupalSettings.view_custom_settings.view_display_id) !== "undefined" &&
        typeof(drupalSettings.view_custom_settings.view_dom_id) !== "undefined") {
        
        const link = $(column).find('.filter-link');
        const term_id = $(link).attr('attribute-term-id');
        // Everything we need to specify about the view.
        const view_info = {
          view_name: drupalSettings.view_custom_settings.view_id,
          view_display_id: drupalSettings.view_custom_settings.view_display_id,
          view_args: term_id,
          view_dom_id: drupalSettings.view_custom_settings.view_dom_id
        };
        // Details of the ajax action.
        const ajax_settings = {
          submit: view_info,
          url: '/views/ajax',
          element: column,
          event: 'click'
        };
  
        Drupal.ajax(ajax_settings);
      }
    }
  };

})(jQuery, Drupal);


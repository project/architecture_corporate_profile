<?php

/**
 * @file
 * Contains hooks and custom functions.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\views\ViewExecutable;

/**
 * Implements hook_help().
 */
function architecture_corporate_common_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.architecture_corporate_common':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Architecture corporate profile.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_views_pre_render().
 */
function architecture_corporate_common_views_pre_render(ViewExecutable $view) {
  // @TODO Think about getting a view_id from the admin settings page
  // @TODO to make this function more flexible.
  if (isset($view) && ($view->storage->id() == 'projects_categories')) {
    $view->element['#attached']['library'][] = 'architecture_corporate_common/architecture_corporate';
    $view->element['#attached']['drupalSettings']['view_custom_settings'] = [
      'view_id' => $view->storage->id(),
      'view_display_id' => $view->current_display,
      'view_dom_id' => $view->dom_id,
    ];
  }
  if (isset($view) && ($view->storage->id() == 'eck_entity_browser')) {
    $view->element['#attached']['library'][] = 'architecture_corporate_common/view';
  }
  if (isset($view) && ($view->storage->id() == 'term_entity_browser')) {
    $view->element['#attached']['library'][] = 'architecture_corporate_common/view';
  }
}

/**
 * Implements hook_field_formatter_third_party_settings_form().
 */
function architecture_corporate_common_field_formatter_third_party_settings_form(FormatterInterface $plugin, FieldDefinitionInterface $field_definition, $view_mode, $form, FormStateInterface $form_state) {
  $element = [];
  $cardinality = 1;
  $target_bundles = [];
  $entity_type = 'paragraph';
  if ($field_definition instanceof FieldConfig) {
    /* @var Drupal\field\Entity\FieldConfig $field_definition */
    $cardinality = $field_definition->getFieldStorageDefinition()->getCardinality();
    $field_type = $field_definition->getFieldStorageDefinition()->getType();
    $target_type = $field_definition->getFieldStorageDefinition()->getSetting('target_type');
    $handler_settings = $field_definition->getSetting('handler_settings');
    if (isset($handler_settings['target_bundles'])) {
      $target_bundles = $handler_settings['target_bundles'];
    }
  }

  // If only one one value is provided the range has no meaning.
  if (($cardinality == 1 || !isset($field_type) || !isset($target_type)) && isset($field_type)
    && $field_type != 'entity_reference_revisions' && $target_type != $entity_type) {
    return $element;
  }

  if (empty($target_bundles)) {
    return $element;
  }

  $element['selected_view_modes'] = [
    '#type' => 'details',
    '#title' => t('View modes for each paragraph'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $element['selected_view_modes']['view_mode_per_paragraph'] = [
    '#type' => 'table',
    '#header' => [
      t('Paragraph'),
      t('View Mode'),
    ],
  ];

  $entity_display = \Drupal::service('entity_display.repository');
  $bundle_info = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type);
  $third_party_setting = $plugin->getThirdPartySetting('architecture_corporate_common', 'selected_view_modes', 0);
  $view_mode_global = $plugin->getSetting('view_mode');

  foreach ($target_bundles as $key => $paragraph_type) {
    if (!in_array($key, $bundle_info)) {
      return $element;
    }
    $view_modes = $entity_display->getViewModeOptionsByBundle($entity_type, $key);
    if (array_key_exists($view_mode_global, $view_modes)) {
      $default_view_mode = $view_mode_global;
    }
    else {
      $default_view_mode = 'default';
    }

    $element['selected_view_modes']['view_mode_per_paragraph'][$key] = [
      '#type' => 'container',
      'title' => [
        '#type' => 'label',
        '#title' => $bundle_info[$key]['label'],
      ],
      'view_mode' => [
        '#type' => 'select',
        '#title' => t('View mode'),
        '#empty_option'  => t('Select view mode'),
        '#options' => $view_modes,
        '#default_value' => $third_party_setting['view_mode_per_paragraph'][$key]['view_mode'] ? $third_party_setting['view_mode_per_paragraph'][$key]['view_mode'] : $default_view_mode,
      ],
    ];
  }

  return $element;
}

/**
 * Implements hook_entity_view_mode_alter().
 */
function architecture_corporate_common_entity_view_mode_alter(&$view_mode, EntityInterface $entity, $context) {
  if (!($entity instanceof ParagraphInterface)) {
    return;
  }

  $parent_entity = $entity->getParentEntity();
  if ($parent_entity instanceof ParagraphInterface) {
    $third_party_setting = [];
    $third_party_setting_name = 'view_mode_per_paragraph';

    $entity_type = $parent_entity->getEntityType()->id();
    $bundle = $parent_entity->getType();

    $entity_view_display = \Drupal::entityTypeManager()->getStorage('entity_view_display')->load($entity_type . '.' . $bundle . '.' . $view_mode);
    // Get third party setting.
    _architecture_corporate_common_search_key($third_party_setting_name, $entity_view_display->get('content'), $third_party_setting);
    if (isset($third_party_setting['view_mode_per_paragraph']) && !empty($third_party_setting['view_mode_per_paragraph'])) {
      foreach ($third_party_setting['view_mode_per_paragraph'] as $setting_key => $setting_value) {
        $current_paragraph_type = $entity->getType();
        if ($setting_key == $current_paragraph_type) {
          $view_mode = $setting_value['view_mode'];
        }
      }
    }
  }
}

/**
 * Helper function to find third party settings.
 *
 * @param string $searchKey
 *   Third party setting name.
 * @param array $arr
 *   Array.
 * @param array $result
 *   Result array.
 */
function _architecture_corporate_common_search_key($searchKey, array $arr, array &$result) {
  if (isset($arr[$searchKey])) {
    $result[$searchKey] = $arr[$searchKey];
  }
  foreach ($arr as $param) {
    if (is_array($param)) {
      _architecture_corporate_common_search_key($searchKey, $param, $result);
    }
  }
}

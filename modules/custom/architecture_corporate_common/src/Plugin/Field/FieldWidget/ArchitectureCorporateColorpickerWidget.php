<?php

namespace Drupal\architecture_corporate_common\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\jquery_colorpicker\Plugin\Field\FieldWidget\JQueryColorpickerWidget;

/**
 * Not default jQuery Colorpicker field widget.
 *
 * @FieldWidget(
 *   id = "architecture_corporate_colorpicker",
 *   label = @Translation("jQuery Colorpicker (Custom)"),
 *   field_types = {
 *      "colorapi_color_field"
 *   }
 * )
 */
class ArchitectureCorporateColorpickerWidget extends JQueryColorpickerWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['name']['#type'] = 'hidden';
    return $element;
  }

}

core = 8.x
api = 2

; Modules

defaults[projects][subdir] = contrib

projects[admin_toolbar][version] = 2.4

projects[backup_migrate][version] = 4.2
projects[backup_migrate][patch][] = https://www.drupal.org/files/issues/2018-09-20/2992448-13-fix_config_file_name.patch

projects[better_exposed_filters][version] = 5.0-beta3

projects[blazy][version] = 2.4

projects[colorapi][version] = 1.1

projects[crop][version] = 2.1

projects[ctools][version] = 3.7

projects[default_content][version] = 1.0-alpha9
projects[default_content][patch][] = "https://www.drupal.org/files/issues/2018-12-11/default_content-dont-reimport-2698425-109.patch"
projects[default_content][patch][] = "https://www.drupal.org/files/issues/entity_does_not-2907594-2.patch"

projects[dropzonejs][version] = 2.5

projects[ds][version] = 3.13

projects[entity][version] = 1.2

projects[entity_browser][version] = 2.6

projects[entity_browser_enhanced][version] = 1.0

projects[entity_reference_revisions][version] = 1.9
projects[entity_reference_revisions][patch][] = "https://www.drupal.org/files/issues/embed_paragraph_content-2848878-43.patch"

projects[entity_usage][version] = 2.0-beta3

projects[field_formatter_range][version] = 1.2

projects[field_group][version] = 3.1
projects[field_group][patch][] = "https://www.drupal.org/files/issues/2019-01-15/field_group-tokens_in_classes-2858336-16.patch"

projects[file_entity][version] = 2.0-beta9

projects[focal_point][version] = 1.5

projects[inline_entity_form][version] = 1.0-rc9

projects[jquery_colorpicker][version] = 2.0-rc2

projects[jquery_ui_touch_punch][download][tag] = 1.0.0

projects[jquery_ui][version] = 1.4

projects[jquery_ui_datepicker][version] = 1.1

projects[jquery_ui_draggable][version] = 1.2

projects[jquery_ui_droppable][version] = 1.2

projects[jquery_ui_slider][version] = 1.1

projects[media_entity_browser][version] = 2.0-alpha3

projects[paragraphs][version] = 1.12

projects[paragraphs_browser][version] = 1.0

projects[paragraphs_browser_previewer][version] = 1.0

projects[paragraphs_previewer][version] = 1.5

projects[pathauto][version] = 1.8

projects[rabbit_hole][version] = 1.0-beta10

projects[redirect][version] = 1.6

projects[restui][version] = 1.20

projects[search404][version] = 1.0

projects[simple_block][version] = 1.2

projects[simple_gmap][download][tag] = 3.0.1

projects[slick][version] = 2.3

projects[slick_media][version] = 3.x-dev

projects[slick_paragraphs][version] = 2.1

projects[slick_views][version] = 2.4

projects[smart_trim][version] = 1.3

projects[social_media_links][version] = 2.8

projects[token][version] = 1.9

projects[ui_patterns][version] = 1.2

projects[ui_patterns_pattern_lab][version] = 1.0-alpha6

projects[vendor_stream_wrapper][version] = 1.6

projects[video_embed_field][version] = 2.4

projects[views_accordion][version] = 1.3

projects[views_infinite_scroll][version] = 1.8

projects[viewsreference][version] = 2.0-beta2

projects[webform][version] = 5.27

; Libraries

libraries[blazy][directory_name] = "blazy"
libraries[blazy][type] = "library"
libraries[blazy][destination] = "libraries"
libraries[blazy][download][type] = "git"
libraries[blazy][download][url] = "https://github.com/dinbror/blazy.git"
libraries[blazy][download][tag] = "1.8.2"

libraries[dropzone][directory_name] = "dropzone"
libraries[dropzone][type] = "library"
libraries[dropzone][destination] = "libraries"
libraries[dropzone][download][type] = "git"
libraries[dropzone][download][url] = "https://github.com/enyo/dropzone.git"
libraries[dropzone][download][tag] = "v5.5.0"

libraries[slick][directory_name] = "slick"
libraries[slick][type] = "library"
libraries[slick][destination] = "libraries"
libraries[slick][download][type] = "git"
libraries[slick][download][url] = "https://github.com/kenwheeler/slick.git"
libraries[slick][download][tag] = "v1.8.1"

libraries[jquery_ui_touch_punch][directory_name] = "jquery-ui-touch-punch"
libraries[jquery_ui_touch_punch][type] = "library"
libraries[jquery_ui_touch_punch][destination] = "libraries"
libraries[jquery_ui_touch_punch][download][type] = "git"
libraries[jquery_ui_touch_punch][download][url] = "https://github.com/furf/jquery-ui-touch-punch.git"

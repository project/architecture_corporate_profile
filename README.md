Architecture Corporate Profile
==================

Architecture corporate profile is built to provide place for artist/art-group to
share their (art)works, projects, collaborations, as well as relevant news,
lectures, etc.

Uses its own custom theme, responsive design included: looks well on desktops,
laptops, tablets and smartphones.

To install the profile:

- Unzip the folder and copy it to the necessary folder of your server.
- Install your new website with the Architecture Corporate Profile. It’s set by
default, you just need to click the “Save and continue” button and follow the
instructions.
- Wait for the Architecture Corporate Profile to be installed and enjoy your
website.

Thank you for choosing the Architecture Corporate Profile!

Created by ADCI Solutions

webpackJsonp([0],[
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__fonts_main_css__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__fonts_main_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__fonts_main_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__scss_main_scss__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__scss_main_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__scss_main_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__js_main__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__js_main___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__js_main__);
/* Vendor */

/* Fonts */


/* Styles */


/* Scripts */


/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
__webpack_require__(4);
__webpack_require__(5);
__webpack_require__(6);
__webpack_require__(7);
__webpack_require__(8);

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var menu = document.getElementsByClassName("mobile-menu")[0];
var menuButton = document.getElementsByClassName("menu-button")[0];
var body = document.getElementsByTagName("body")[0];
var x0 = null;
var x1 = null;
var y0 = null;
var y1 = null;
var deltaX = null;
var deltaY = null;

function openMobileMenu() {
  menu.classList.add("is-open");
  body.classList.add("menu-open");
}

function hideMobileMenu(e) {
  if (menu.classList.contains("is-open") && e.target === body) {
    menu.classList.remove("is-open");
    body.classList.remove("menu-open");
  }
}

/* Hide and open mobile menu with swipe */

function getTouchStart(e) {
  x0 = e.touches[0].clientX;
  y0 = e.touches[0].clientY;
}

function getTouchEnd(e) {
  x1 = e.changedTouches[0].clientX;
  y1 = e.changedTouches[0].clientY;
  deltaX = x1 - x0;
  deltaY = Math.abs(y1 - y0);
}

function touchClose(e) {
  getTouchEnd(e);
  if (deltaX < -30 && deltaY < 20) {
    menu.classList.remove("is-open");
    body.classList.remove("menu-open");
  }
}

function touchOpen(e) {
  getTouchEnd(e);
  var notSlider = e.target.closest(".slick__slider") === null;
  var notMap = e.target.closest(".paragraph--map-element") === null;
  var smallWindow = window.innerWidth < 768;
  if (smallWindow && deltaX > 50 && deltaY < 20 && notSlider && notMap) {
    openMobileMenu();
  }
}

menuButton.addEventListener("click", openMobileMenu);
body.addEventListener("click", hideMobileMenu);
menu.addEventListener("touchstart", getTouchStart, false);
menu.addEventListener("touchmove", touchClose, false);
body.addEventListener("touchstart", getTouchStart);
body.addEventListener("touchmove", touchOpen);

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var searchMobile = document.querySelector(".region-header-right");
var searchButton = document.querySelector(".search-button");

var hideSearch = function hideSearch(e) {
  if (e.target.classList.contains("is-open")) {
    e.target.classList.remove("is-open");
  }
};

var showSearch = function showSearch() {
  if (!searchMobile.classList.contains("is-open")) {
    searchMobile.classList.add("is-open");
  }
};

searchButton.addEventListener("click", showSearch);
searchMobile.addEventListener("click", hideSearch);

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
if (document.getElementsByClassName("container-type--four_column")[0]) {
  var fourColumn = document.getElementsByClassName(
  "container-type--four_column")[
  0];
  var fourColumnBackgound = fourColumn.style["background-color"];

  if (fourColumnBackgound === "rgb(255, 255, 255)") {
    fourColumn.style.color = "#000";
  }
}

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var skills = document.querySelectorAll(
".paragraph--scale--scale .field--name-field-number");

var skillLabels = document.getElementsByClassName("paragraph--scale--label");

function drawSkills() {
  for (var i = 0; i < skills.length; i++) {
    var skillLevel = parseFloat(skills[i].innerText);
    if (typeof skillLevel === "number" && 0 <= skillLevel <= 100) {
      skills[i].classList.add("animated");
      skills[i].style.width = skillLevel + "%";
      skillLabels[i].innerText = skillLevel + "%";
    }
  }
}

function resetSkills() {
  for (var i = 0; i < skills.length; i++) {
    skills[i].classList.remove("animated");
    skills[i].style.width = 0;
  }
}

/***
  
  The next function is based on: https://eddyerburgh.me/animate-elements-scrolled-view-vanilla-js
  
  ***/

var animateSkills = function animateSkills() {
  var skillsBlock = void 0;
  var windowHeight = void 0;
  function init() {
    skillsBlock = document.getElementsByClassName("container-type--skills")[0];
    windowHeight = window.innerHeight;
    addEventHandlers();
    checkPosition();
  }
  function addEventHandlers() {
    window.addEventListener("scroll", checkPosition);
    window.addEventListener("resize", init);
  }
  function checkPosition() {
    if (skillsBlock !== undefined) {
      var positionFromTop = skillsBlock.getBoundingClientRect().top;
      if (positionFromTop - windowHeight <= 0) {
        drawSkills();
      } else {
        resetSkills();
      }
    }
  }
  return {
    init: init };

};

animateSkills().init();

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
 /* Next code is based on: 
              https://pageclip.co/blog/2018-02-20-you-should-use-html5-form-validation.html */

var invalidClassName = "invalid";
var inputs = document.querySelectorAll("input, select, textarea");
inputs.forEach(function (input) {
  // Add a css class on submit when the input is invalid.
  input.addEventListener("invalid", function () {
    input.classList.add(invalidClassName);
  });

  // Remove the class when the input becomes valid.
  // 'input' will fire each time the user types
  input.addEventListener("input", function () {
    if (input.validity.valid) {
      input.classList.remove(invalidClassName);
    }
  });
});

/***/ })
],[0]);
//# sourceMappingURL=main.js.map
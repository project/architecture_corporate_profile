const searchMobile = document.querySelector(".region-header-right");
const searchButton = document.querySelector(".search-button");

const hideSearch = e => {
  if (e.target.classList.contains("is-open")) {
    e.target.classList.remove("is-open");
  }
};

const showSearch = () => {
  if (!searchMobile.classList.contains("is-open")) {
    searchMobile.classList.add("is-open");
  }
};

searchButton.addEventListener("click", showSearch);
searchMobile.addEventListener("click", hideSearch);

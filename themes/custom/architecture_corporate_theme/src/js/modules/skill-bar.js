let skills = document.querySelectorAll(
  ".paragraph--scale--scale .field--name-field-number"
);
let skillLabels = document.getElementsByClassName("paragraph--scale--label");

function drawSkills() {
  for (let i = 0; i < skills.length; i++) {
    let skillLevel = parseFloat(skills[i].innerText);
    if (typeof skillLevel === "number" && 0 <= skillLevel <= 100) {
      skills[i].classList.add("animated");
      skills[i].style.width = skillLevel + "%";
      skillLabels[i].innerText = skillLevel + "%";
    }
  }
}

function resetSkills() {
  for (let i = 0; i < skills.length; i++) {
    skills[i].classList.remove("animated");
    skills[i].style.width = 0;
  }
}

/***

The next function is based on: https://eddyerburgh.me/animate-elements-scrolled-view-vanilla-js

***/

let animateSkills = function() {
  let skillsBlock;
  let windowHeight;
  function init() {
    skillsBlock = document.getElementsByClassName("container-type--skills")[0];
    windowHeight = window.innerHeight;
    addEventHandlers();
    checkPosition();
  }
  function addEventHandlers() {
    window.addEventListener("scroll", checkPosition);
    window.addEventListener("resize", init);
  }
  function checkPosition() {
    if (skillsBlock !== undefined) {
      let positionFromTop = skillsBlock.getBoundingClientRect().top;
      if (positionFromTop - windowHeight <= 0) {
        drawSkills();
      } else {
        resetSkills();
      }
    }
  }
  return {
    init: init
  };
};

animateSkills().init();

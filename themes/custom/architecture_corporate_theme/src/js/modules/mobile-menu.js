let menu = document.getElementsByClassName("mobile-menu")[0];
let menuButton = document.getElementsByClassName("menu-button")[0];
let body = document.getElementsByTagName("body")[0];
let x0 = null;
let x1 = null;
let y0 = null;
let y1 = null;
let deltaX = null;
let deltaY = null;

function openMobileMenu() {
  menu.classList.add("is-open");
  body.classList.add("menu-open");
}

function hideMobileMenu(e) {
  if (menu.classList.contains("is-open") && e.target === body) {
    menu.classList.remove("is-open");
    body.classList.remove("menu-open");
  }
}

/* Hide and open mobile menu with swipe */

function getTouchStart(e) {
  x0 = e.touches[0].clientX;
  y0 = e.touches[0].clientY;
}

function getTouchEnd(e) {
  x1 = e.changedTouches[0].clientX;
  y1 = e.changedTouches[0].clientY;
  deltaX = x1 - x0;
  deltaY = Math.abs(y1 - y0);
}

function touchClose(e) {
  getTouchEnd(e);
  if (deltaX < -30 && deltaY < 20) {
    menu.classList.remove("is-open");
    body.classList.remove("menu-open");
  }
}

function touchOpen(e) {
  getTouchEnd(e);
  let notSlider = e.target.closest(".slick__slider") === null;
  let notMap = e.target.closest(".paragraph--map-element") === null;
  let smallWindow = window.innerWidth < 768;
  if (smallWindow && deltaX > 50 && deltaY < 20 && notSlider && notMap) {
    openMobileMenu();
  }
}

menuButton.addEventListener("click", openMobileMenu);
body.addEventListener("click", hideMobileMenu);
menu.addEventListener("touchstart", getTouchStart, false);
menu.addEventListener("touchmove", touchClose, false);
body.addEventListener("touchstart", getTouchStart);
body.addEventListener("touchmove", touchOpen);
